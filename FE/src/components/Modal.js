import {useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import {getDownloadURL, ref , uploadBytes} from "firebase/storage"
import { storage } from "../config/firebase/firebase.config";

function MyVerticallyCenteredModal(props) {
  const [fileFoto, setFileFoto] = useState(null);
  // eslint-disable-next-line no-unused-vars
  const [firebaseFotoPath, setFirebaseFotoPath] = useState(null)

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="fw-600 text-dark"
        >
          Edit Profile Foto
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <label htmlFor="formFile" className="form-label">
          Choose your profile foto
        </label>
        <input
          type="file"
          className="form-control"
          id="formFile"
          onChange={(e) => {
            setFileFoto(e.target.files[0]);
          }}
        />
        <button
          className="btn btn-primary mt-4"
          onClick={async() => {
            const productImagesRef = ref(storage, 'images/profile-images');
            const assetPath = `${productImagesRef}/${fileFoto.name}`;
            const imageRef = ref(storage,assetPath)
            const path = await uploadBytes(imageRef,fileFoto)
            console.log(path)
            getDownloadURL(imageRef).then((res)=>{
                setFirebaseFotoPath(res)
                console.log(res)
            })
          }}
        >
          Upload
        </button>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default MyVerticallyCenteredModal;
