/* eslint-disable no-cond-assign */
/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import profile from "../../assets/icons/profile-icon.svg";
import axios from "axios";
import jwt_decode from "jwt-decode";
import MyVerticallyCenteredModal from "../../components/Modal";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";

function LobbyGame() {
  // eslint-disable-next-line no-unused-vars
  const [roomsData, setRoomsData] = useState([]);
  const [errMsg, setErrMsg] = useState("");
  const [loading, setLoading] = useState(true);
  const [loading2, setLoading2] = useState(true);
  const [isNull, setIsNull] = useState(false);
  const [value, setValue] = useState(false);
  const [biodata, setBiodata] = useState({
    fullname: null,
    address: null,
    phoneNumber: null,
    dateOfBirth: null,
  });
  const [inputBiodata, setInputBiodata] = useState({});
  const [page, setPage] = useState(1);

  const accessToken = localStorage.getItem("accessToken");
  const authorization = accessToken.split(" ")[1];
  const token = jwt_decode(authorization);

  const getAllRooms = async () => {
    try {
      const data = await axios({
        method: "get",
        url: `https://backend-team-1-five.vercel.app/all_rooms/${page}`,
        headers: { Authorization: `${accessToken}` },
      }).then((response) => {
        return response.data.message;
      });
      const restructuredData = [];
      data.map((roomData) => {
        if (roomData.resultGames === undefined) {
          return restructuredData.push({
            roomId: roomData.roomId,
            roomName: roomData.roomName,
            player1Id: roomData.player1Games.player1Id,
            player1Choice: roomData.player1Choice,
            player1Name: roomData.player1Games.player1Name,
            player1Status: roomData.resultGames?.[0].status,
            player2Id: roomData.player2Games?.player2Id,
            player2Choice: roomData.player2Choice,
            player2Name: roomData.player2Games?.player2Name,
            player2Status: roomData.resultGames?.[1].status,
          });
        } else {
          if (
            roomData.player1Games.player1Id ===
            roomData.resultGames[0]?.playerId
          ) {
            return restructuredData.push({
              roomId: roomData.roomId,
              roomName: roomData.roomName,
              player1Id: roomData.player1Games.player1Id,
              player1Choice: roomData.player1Choice,
              player1Name: roomData.player1Games.player1Name,
              player1Status: roomData.resultGames?.[0]?.status,
              player2Id: roomData.player2Games?.player2Id,
              player2Choice: roomData.player2Choice,
              player2Name: roomData.player2Games?.player2Name,
              player2Status: roomData.resultGames?.[1]?.status,
            });
          } else {
            return restructuredData.push({
              roomId: roomData.roomId,
              roomName: roomData.roomName,
              player1Id: roomData.player1Games.player1Id,
              player1Choice: roomData.player1Choice,
              player1Name: roomData.player1Games.player1Name,
              player1Status: roomData.resultGames?.[1]?.status,
              player2Id: roomData.player2Games?.player2Id,
              player2Choice: roomData.player2Choice,
              player2Name: roomData.player2Games?.player2Name,
              player2Status: roomData.resultGames?.[0]?.status,
            });
          }
        }
      });
      setLoading(false);
      if (restructuredData.length < 1) {
        setIsNull(true);
        setRoomsData(restructuredData);
      } else {
        setIsNull(false);
        setRoomsData(restructuredData);
      }
    } catch (error) {
      alert(error);
    }
  };

  const getBiodata = async () => {
    try {
      const biodatas = await axios
        .get(
          `https://backend-team-1-five.vercel.app/profile/${token.id.toString()}`,
          {
            headers: { Authorization: `${accessToken}` },
          }
        )
        .then((response) => {
          return response.data.message;
        });
      setBiodata({
        ...biodata,
        fullname: biodatas.fullname,
        address: biodatas.address,
        phoneNumber: biodatas.phoneNumber,
        dateOfBirth: biodatas.dateOfBirth,
      });
      setInputBiodata({
        ...inputBiodata,
        fullname: biodatas.fullname,
        address: biodatas.address,
        phoneNumber: biodatas.phoneNumber,
        dateOfBirth: biodatas.dateOfBirth,
      });
      setLoading2(false);
    } catch (error) {
      setBiodata({
        ...biodata,
        fullname: null,
        address: null,
        phoneNumber: null,
        dateOfBirth: null,
      });
      setLoading2(false);
    }
  };

  useEffect(() => {
    getAllRooms();
  }, [page]);

  useEffect(() => {
    getBiodata();
  }, []);

  const editClick = () => {
    setValue(true);
  };
  const updateClick = async () => {
    try {
      const response = await axios({
        method: "put",
        url: `https://backend-team-1-five.vercel.app/profile/${token.id.toString()}`,
        headers: { Authorization: `${accessToken}` },
        data: inputBiodata,
      });
      setErrMsg(response.data.message);
      getBiodata();
      setValue(false);
      alert("Update biodata success");
    } catch (error) {
      alert(JSON.stringify(error.response.data.message));
    }
  };

  const handlePreviousClick = () => {
    if (page >= 1) {
      setLoading(true);
      setPage(page - 1);
    } else {
      setPage(page);
    }
  };

  const [modalShow, setModalShow] = React.useState(false);
  const handleEditFoto = () => {
    setModalShow(true);
  };

  const handleNextClick = () => {
    if (page >= 1) {
      setLoading(true);
      setPage(page + 1);
    } else {
      setPage(page);
    }
  };
  return (
    <div className="bigContainer">
      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
      <div className="left-container">
        <div className="pvcGame">
          <div className="playVCom">
            <Link className="btnVCom" to="/lobbygame/playervscom">
              Player Vs Com
            </Link>
          </div>
          <div className="playVCom">
            <Link className="btnVCom" to="/lobbygame/createRoom">
              Create Room PVP
            </Link>
          </div>
        </div>
        <div className="roomTitle title mx-4">Rooms : </div>
        <div className="roomContainer">
          {roomsData !== null ? (
            <div className="row ms-lg-3 ms-sm-2 mt-sm-0 mt-lg-0">
              {loading !== true ? (
                roomsData.map((roomData) => {
                  return (
                    <div className="dataCourier" key={roomData.roomId}>
                      <Link
                        className={
                          roomData.player2Name !== undefined
                            ? "courierText closed"
                            : "courierText open"
                        }
                        to={
                          roomData.player2Name !== undefined
                            ? "/lobbygame/p1vsp2closed"
                            : "/lobbygame/p1vsp2"
                        }
                        state={{ value: roomData }}
                        onClick={() => {
                          console.log(roomData);
                        }}
                      >
                        <>Room: {roomData.roomName}</>
                        <br></br>
                        <div className="">
                          Status:{" "}
                          {roomData.player2Name !== undefined
                            ? "closed"
                            : "open"}
                        </div>
                      </Link>
                      <div className="activePanel">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill={
                            roomData.player2Name !== undefined
                              ? "#ff0000"
                              : "#33ff40"
                          }
                          viewBox="0 0 16 16"
                        >
                          <circle cx="6" cy="6" r="6" />
                        </svg>
                      </div>
                    </div>
                  );
                })
              ) : (
                <h3>Loading...</h3>
              )}
            </div>
          ) : (
            <>Room Not Found</>
          )}
        </div>
        <div className="col-md-12 col-sm-12 d-flex wrap mt-xl-5 mt-lg-5 mt-sm-3 justify-content-evenly align-items-center">
          <Link
            onClick={handlePreviousClick}
            className={
              page !== 1 || isNull === true
                ? "paginate fs-5 me-2 fs-sm-6 text-center"
                : "paginate previousClick fs-5 fs-sm-6 me-2 text-center"
            }
          >{`<< Previous`}</Link>
          <Link
            onClick={handleNextClick}
            className={
              page >= 1 && isNull === false
                ? "paginate fs-5 ms-2 fs-sm-6 text-center"
                : "paginate nextClick fs-5 ms-lg-2 ms-md-2 fs-sm-6 text-center"
            }
          >{`Next >>`}</Link>
        </div>
      </div>
      <div className="right-container px-xl-4 px-lg-4 px-sm-3">
        <div className="upper-right-container mt-1 mx-4">
          <div className="top-upper-right-container mt-2 pe-2">
            <Link
              className={
                value !== true
                  ? "gameHistoryText text-dark text-center text-decoration-underline me-4"
                  : "gameHistoryText text-dark text-center text-decoration-underline me-5 ms-0"
              }
              to={"/lobbygame/gamehistory"}
            >
              Game History
            </Link>
            <i
              className={value !== true ? "editIcons fa fa-pencil" : null}
              aria-hidden="true"
              onClick={editClick}
            />
          </div>
          <img
            src={profile}
            alt="profile"
            className="rounded-circle mt-1 mt-sm-0"
            style={{ cursor: "pointer" }}
            onClick={handleEditFoto}
          />
          <div className="profileTitle title">{token.username}</div>
        </div>
        <div
          className={
            value !== true
              ? "updatedContainer lower-right-container"
              : "lower-right-container row mt-2 pb-5 pt-2 ps-4"
          }
        >
          {loading2 !== true ? (
            <>
              <div className="biodata">
                Fullname:{" "}
                {biodata.fullname === null ? "....." : biodata.fullname}
              </div>
              <div className="biodata">
                Address: {biodata.address === null ? "....." : biodata.address}
              </div>
              <div className="biodata">
                Phone:{" "}
                {biodata.phoneNumber === null ? "....." : biodata.phoneNumber}
              </div>
              <div className="biodata">
                Date of Birth:{" "}
                {biodata.dateOfBirth === null ? "....." : biodata.dateOfBirth}
              </div>
            </>
          ) : (
            <h4>Loading...</h4>
          )}
        </div>
        <div
          className={
            value !== true
              ? "notUpdated lower-right-container-input"
              : "lower-right-container-input formBiodata mt-xl-2 mt-lg-2 ps-lg-4 ps-sm-0 mt-sm-0"
          }
        >
          <div className="commandInput text-center text-capitalize fw-regular fs-6 text-dark mb-1 me-5 mb-sm-2 me-sm-2">
            Please input your biodata
          </div>
          <label htmlFor="fullnameInput" className="labelInput fs-6 mt-0">
            Fullname:
          </label>
          <input
            type="text"
            defaultValue={biodata.fullname}
            placeholder="Vito Corleone"
            className="inputBiodata inputData mt-0"
            id="fullnameInput"
            disabled={false}
            onChange={(e) => {
              setInputBiodata({ ...inputBiodata, fullname: e.target.value });
            }}
          />
          <label htmlFor="addressInput" className="labelInput fs-6 mt-0">
            Address:
          </label>
          <input
            type="text"
            defaultValue={biodata.address}
            placeholder="Corleone Sicily"
            className="inputBiodata inputData mt-0"
            id="addressInput"
            disabled={false}
            onChange={(e) => {
              setInputBiodata({ ...inputBiodata, address: e.target.value });
            }}
          />
          <label htmlFor="phoneNumber" className="labelInput fs-6 mt-0">
            Phone Number:
          </label>
          <input
            type="text"
            defaultValue={biodata.phoneNumber}
            placeholder="082233178123"
            className="inputBiodata inputData mt-0"
            id="phoneNumber"
            disabled={false}
            onChange={(e) => {
              setInputBiodata({ ...inputBiodata, phoneNumber: e.target.value });
            }}
          />
          <label htmlFor="dateOfBrith" className="labelInput fs-6 mt-0">
            Date of Birth:
          </label>
          <input
            type="text"
            defaultValue={biodata.dateOfBirth}
            placeholder="07-12-1891"
            className="inputBiodata inputData mt-0"
            id="dateOfBrith"
            disabled={false}
            onChange={(e) => {
              setInputBiodata({ ...inputBiodata, dateOfBirth: e.target.value });
            }}
          />
          <button
            className="updateBiodata button ms-5 py-3 ms-sm-5 py-sm-1"
            onClick={updateClick}
          >
            update Biodata
          </button>
        </div>
        <div className={errMsg ? "errMsg" : "offscreen"} aria-live="assertive">
          {errMsg}
        </div>
      </div>
    </div>
  );
}

export default LobbyGame;
